# 《大学生软件测试大赛》辅导-- 开发者测试：第二讲
## 如何在 WebIDE 测试一个Java项目？  （以 nextday 为例）
## 1. 第一部分：基本流程 （4步）
1. 在项目文件夹 mootest 新建一个测试类（文件）
 + 如下面图所示
 
   图1
   
 ![图1](./pic/1.jpeg)
 
 + 点击下面图2中的【+】，添加文件。保存文件，建议文件名为 ****Test.java
 
   图2 
   
   ![图2](./pic/2.jpeg)

2. 编写测试类。

 + 如下面图所示
 
  图3
  ![图3](./pic/3.jpeg)

3. 运行测试类，查看成绩
   + 运行测试类，如下图：
   
   图4 
   ![图4](./pic/4.jpeg)
   
   + 查看成绩，如下图：
   
   图5 ![图5](./pic/5.png)

4. 提交本次比赛（练习）成绩。
   + 如下图
   
   图6 
   ![图6](./pic/6.jpeg)


## 2. 第二部分：测试用例的编写 （4步）
1. 需要建多少个测试类？
  + 在项目文件夹 mootest 新建测试类（文件）。首先，你得读懂程序的功能，这个程序功能很好懂。先看看都用哪些待测文件，可以浏览 Web IDE 的左边。
  + 点击上面图2中的【+】，添加文件。保存文件，建议文件名为 ****Test.java
  + 如下面图所示
 
   图7
   ![图1](./pic/21.jpeg)

2. 编写测试类。

 + 哪些包必须需要引入？———— 类头部内容，包括4个部分。如下面图所示
  
   图8
   ![图2](./pic/22.jpeg)
 
 + 类主体内容，包括测试数据准备+ 测试运行验证。

   - 测试数据准备，如下面图所示
 
    图9
    ![图3](./pic/23.jpeg)
    
   - 测试脚本，如下面图所示
    
    图10 
   ![图4](./pic/24.jpeg)
## 3. 第三部分：附上本次讲解的代码 （NextdayTest.java）

```
/**
 * 
 */
package net.mooctest;

import static org.junit.Assert.*;

import org.junit.Test;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Collection;

@RunWith(value = Parameterized.class)


public class NextdayTest {

	private Date expected;
	
	private Date value;
	
	@Parameters
	public static Collection<Object[]> getTestParameters() {
		
		Date d1 = new Date(5, 14, 2014);
		Date d2 = new Date(5, 13, 2014);
		
		//31号
		Date d7 = new Date(8, 31, 2015);
		Date d8 = new Date(8, 30, 2015);
		
		Date d9 = new Date(7, 31, 2014);
		Date d10 = new Date(7, 30, 2014);
		
		Date d11 = new Date(7, 1, 2013);
		Date d12 = new Date(6, 30, 2013);
		
		//闰年
		Date d13 = new Date(3, 1, 2004);  
		Date d14 = new Date(2, 29, 2004);
		
		Date d15 = new Date(2, 29, 2004);
		Date d16 = new Date(2, 28, 2004);
		
		//400年是闰年
		Date d17 = new Date(3, 1, 2000);
		Date d18 = new Date(2, 29, 2000);
		
		Date d19 = new Date(3, 1, 2013);
		Date d20 = new Date(2, 28, 2013);
		
		//被4整除，不被100整除 
		Date d23 = new Date(3, 1, 2100);
		Date d24 = new Date(2, 28, 2100);
		
		//到第二年
		Date d21 = new Date(1, 1, 2014);
		Date d22 = new Date(12, 31, 2013);
		
		//下一个月
		Date d25 = new Date(1, 31, 2013);
		Date d26 = new Date(1, 30, 2013);
		
		Date d27 = new Date(2, 1, 2013);
		Date d28 = new Date(1, 31, 2013);
		
		Date d29 = new Date(4, 1, 2014);
		Date d30 = new Date(3, 31, 2014);
		
		Date d31 = new Date(5, 1, 2014);
		Date d32 = new Date(4, 30, 2014);

		Date d33 = new Date(6, 1, 2014);
		Date d34 = new Date(5, 31, 2014);

		Date d35 = new Date(7, 1, 2014);
		Date d36 = new Date(6, 30, 2014);
		
		
		
		Object[][] date = new Object[][] {
				{d1, d2},
				{d7, d8},
				{d9, d10},
				{d11, d12},
				{d13, d14},
				{d15, d16},
				{d17, d18},
				{d19, d20},
				{d21, d22},
				{d23, d24},
				{d25, d26},
				{d27, d28},
				{d29, d30},
				{d31, d32},
				{d33, d34},
				{d35, d36},
//				{d3, d4},
//				{d5, d6}
		};
		return Arrays.asList(date);
			
	}	
	
	
	public NextdayTest(Date expected, Date value) {
		super();
		this.expected = expected;
		this.value = value;
	}
```


