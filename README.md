# 《*Software Testing*》 Agenda
# --- **一、理论部分**
## 第 1 讲： 软件测试概述 （3 学时）
1. 课程介绍（[老师](/Slides/个人介绍.ppt)、[开篇](/Slides/00开篇.ppt)、[课程知识点](/Slides/01软件测试课程知识导图.pptx)、课程教改情况、交流渠道）。[slides]()  [这里 ppt 太多，有点乱，需要整合为1个（或者之间建立链接）]
2. 软件测试概述。[slides](/Slides/Ch1-引论.pptx)
3. Practice & Discuss：[Mooctest 介绍](http://www.mooctest.net/faq) 。[Video]()
    - 现场注册一名学生、加入群组、接受任务
    - Q1：大家平时怎么使用软件的？
    - Q2：遇到Bug一般怎么处理的？
    - Q3：学习中遇到问题，怎么向别人请教？[提问的艺术](http://www.cnblogs.com/juking/p/4893579.html)
4. 作业：课程准备 Mooctest注册、加入群组（群名：ntu，群号：144，群主：鞠小林）、简单测试练习、论坛使用。 [Practice]()

## 第 2 讲： 软件测试入门 （3 学时）
1. 软件测试基本概念 [slides](/Slides/Ch2-软件测试基本概念.pptx)
2. 简单的测试技术："基于Junit的测试" [slides](/Slides/实验一_JUnit介绍及单元测试实验.ppt)
3. Practice & Discuss：
   - 客户端配置 ：[Mooctest论坛](http://www.mooctest.net/faq) --》 开发者测试 DEV ->客户端安装 [Video](http://172.20.34.22/learn/video/software_testing/3 - 3 - D2.1 JUnit-Demo.mp4)
   - Q1：软测测试与软件质量的关系。 [Video](http://172.20.34.22/learn/video/software_testing/测试达人话测试.mp4)
   - Q2：模块测试与系统测试的关系。 [Video](http://172.20.34.22/learn/video/software_testing/)
4. 作业：Mooctest 练习（简单JUnit测试）。 [Practice]()
   - 要求：2周时间完成

## 第 3 讲： 软件测试方法 （6 学时）
1. 软件测试方法 [slides](/Slides/Ch3-软件测试方法.pptx)
2. Web 功能测试（[Selenium](http://www.51testing.com/zhuanti/selenium.html)） [video]()
   - 需求 JDK 8 ，客户端配置 ：[Mooctest论坛](http://www.mooctest.net/faq)  --》Web功能测试
   - 有关JDK 多版本安装切换 [Mac](http://www.cnblogs.com/juking/p/7201247.html) \ [Windows](http://www.cnblogs.com/juking/p/4891931.html) \ [Windows 7](http://www.cnblogs.com/juking/p/6071477.html) 
3. Practice & Discuss：
   - 继续开发者测试。 [Video]()
   - 配置Web功能测试环境。 [Video](http://172.20.34.22/learn/video/software_testing/5%20-%2010%20-%20D4.3%20Selenium-Demo.mp4)
   - Q1：应该怎么设计测试用例？视频集
     - 随机测试 [video](http://172.20.34.22/learn/video/software_testing/5%20-%201%20-%20L4.1%20%e9%9a%8f%e6%9c%ba%e6%b5%8b%e8%af%95%20(Random%20Testing).mp4)
     - 等价类划分 [video](http://172.20.34.22/learn/video/software_testing/5%20-%205%20-%20L4.3%20%e7%ad%89%e4%bb%b7%e7%b1%bb%e5%88%92%e5%88%86%20(Equivalence%20Partition).mp4)
     - 边界值分析 [video](http://172.20.34.22/learn/video/software_testing/5%20-%206%20-%20L4.4%20%e8%be%b9%e7%95%8c%e5%80%bc%e5%88%86%e6%9e%90%20(Boundary-Value%20Analysis).mp4)
     - 决策表 [video](http://172.20.34.22/learn/video/software_testing/5%20-%207%20-%20L4.5%20%e5%86%b3%e7%ad%96%e8%a1%a8%20(Decision%20Table).mp4)
     - 组合测试 [video](http://172.20.34.22/learn/video/software_testing/5%20-%208%20-%20L4.6%20%e7%bb%84%e5%90%88%e6%b5%8b%e8%af%95%20(Combinatorial%20Testing).mp4)
     
     
4. 作业：Mooctest 练习（开发者测试（高级））。 [Practice]()

## 第 4 讲： 单元测试与集成测试 （3 学时）-- 国庆假自由练习
1. 单元测试 [slides](/Slides/Ch5-单元测试与集成测试.pptx)
2. 集成测试 [slides](/Slides/Ch5-单元测试与集成测试.pptx)
3. 探索式测试[补充] [slides]() 
    + [NotePad 测试 video1](http://172.20.34.22/)
    + [Widows 写字板测试 video2](http://172.20.34.22/)
3. Practice & Discuss：
   - 单元测试示例。 [Video]()
   - Q1：单元测试能不能排除所有单元内部的逻辑 bug？why？ 
   - Q2：资源泄漏问题：
     + （1） 摄像头、传感器等 在任务从前台切换到后台时候有没有释放？
     + （2） 屏幕占用问题：任务切换时有没有释放？
4. 作业：Mooctest 练习（开发者测试（随机练习））。 [Practice]()

## 第 5 讲： 系统测试 （6 学时）
1. 系统测试 [slides](/Slides/Ch6-系统测试.pptx)
2. 性能测试JMeter [slides](/Slides/实验二_JMeter介绍 及性能测试实验.pptx)
3. Practice & Discuss：
   - 航空购票网站测试。 [Video](http://172.20.34.22/learn/video/software_testing/D4.5%20Selenium+PICT-Demo.mp4)
   - Q1：性能测试（工具、测试脚本等）会不会影响性能测试的结果？
   - Q2：对响应时间测试？  模拟， 真实？ 存在干扰？
4. 作业：Mooctest 练习（性能测试）。 [Practice]()

## 第 6 讲： 验收测试 （3 学时）
1. 验收测试 [slides](/Slides/Ch7-验收测试.pptx)
2. 易用性（界面设计）、可安装（卸载）
3. Practice & Discuss：
   - Q1：南通大学教务管理系统微信号的易用性、改进建议。 [Video]()
   - Q2：
4. 作业：待定。 [Practice]()

## 第 7 讲： 国际化与本地化（3学时）
1. 软件国际化 [slides](/Slides/Ch8-国际化和本地化测试.pptx)
2. 软件本地化 [slides](/Slides/Ch8-国际化和本地化测试.pptx)
3. Practice & Discuss：
   - Q1: 如何开发国际化软件；
   - Q2: 软件本土化策略与建议。
4. 作业：待定。 [Practice]()

## 第 8 讲： 软件测试自动化及其框架（3学时）
1. 自动化测试框架 [slides](/Slides/Ch9-软件测试自动化及其框架.pptx)
2. 软件性能测试方法 [slides]()
3. 软件安全测试方法 [slides]()
4. 软件界面测试方法 [slides]()
5. Practice & Discuss：
    - Jekins。 [Video]()
    - Q1：软件测试的 Oracle 困境、缓解策略 [video](http://172.20.34.22/learn/video/software_testing/6%20-%204%20-%20L5.4%20%e6%b5%8b%e8%af%95%e9%a2%84%e8%a8%80%20(Test%20Oracle).mp4)
6. 作业：待定。 [Practice]()

## 第 9 讲： 测试环境部署（3学时）
1. 测试环境 [slides](/Slides/Ch12-部署测试环境.pptx)
2. 持续集成测试环境 [slides](/Slides/Ch 12-1 持续集成(精讲).ppt)
3. Practice & Discuss：
   - Jekins。 [Video]()
   - Q1：CI 需要什么要素？
   - Q2：CI 不适用的场景？
4. 作业：待定。 [Practice]()

## 第 10 讲： 测试用例设计与维护（3学时）
1. 软件自动化测试 [slides](/Slides/Ch11-测试用例设计.pptx)
2. 软件开发流程 [slides]()
3. Practice & Discuss：
   - Jekens 。 [Video]()
   - Q1：持续集成中，用例的维护？
   - Q2：对界面的静态分析测试很少？
   - Q3：怎样从自然语言 ---》 约束？
4. 作业：待定。 [Practice]()

## 第 11 讲： 测试执行与缺陷报告（3学时）
1. 缺陷报告跟踪 [slides](/Slides/Ch13-测试执行与缺陷报告、跟踪.pptx)
2. 改进用户体验 [slides]() [趋势移动用户体验测试 Video](http://172.20.34.22/learn/video/software_testing/8.8%20%20%e8%b6%8b%e5%8a%bf%e7%a7%bb%e5%8a%a8%e7%94%a8%e6%88%b7%e4%bd%93%e9%aa%8c%e6%b5%8b%e8%af%95.mp4)
3. Practice & Discuss：
   - Git Issuse。 [Video]()
4. 作业：待定。 [Practice]()

## 第 12 讲： 软件测试与软件质量度量（3学时）
1. 软件度量 [slides](/Slides/Ch14-测试与质量分析报告.pptx)
2. 软件维护 [slides]()
3. 软件开发中创新的思考 [slides]()
3. Practice & Discuss：
   - Q1：软件测试是不是度量？ [Video]()
   - Q2：好软件是怎样（炼成）的？
   - Q3：软件的好坏会不会因用户而异？萝卜与白菜？
4. 作业：待定。 [Practice]()


# **二、实验部分**
## 参考教材：《软件测试实验教程》清华出版社 朱少民
## 1. 单元测试与集成测试部分
### 实验A. 语句和判定覆盖实验
1. sss
2. sss
3. sss
### 实验B. 基于JUnit的单元测试实验
1. www
2. www
3. www
### 实验C. 基于Jenkins的集成测试
1. www
2. www
3. www

## 2. Web应用的系列测试实验
### 实验A. Web应用的功能测试
1. sss
2. sss
3. sss
### 实验B. Web应用的性能测试
1. www
2. www
3. www
### 实验C. Web应用的安全测试
1. www
2. www
3. www

## 3. 移动App 的系列测试实验
### 实验A. 移动App的功能测试和兼容性测试
1. sss
2. sss
3. sss
### 实验B. 移动App的功能测试
1. www
2. www
3. www
### 实验C. 移动App的功能测试
1. www
2. www
3. www


